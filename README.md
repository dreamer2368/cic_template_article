**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

type following commands on bash/unix(mac terminal) shell.

---

## Clone a repository

git clone https://dreamer2368@bitbucket.org/dreamer2368

---

## Update the local repository (only on your personal computer)

1. Add changed files to commit list.  
git add **filenames**
2. Commit the list with a comment.  
git commit -m '**comment**'

---

## Update local changes to the global repository (on bitbucket server)

1. All your change must be committed on the local repository.
2. Push the local repository to the global repository.  
git push origin master

---

## Update from the global repository.

git pull origin master

---

## Check the status of local repository

git status

